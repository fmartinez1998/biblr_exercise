import os
from biblr import biblr
import unittest
import tempfile
import flask

from flask import (
        Flask, request, session, g, redirect, url_for, abort,
        render_template, flash
)
class BiblrTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, flaskr.app.config['DATABASE'] = tempfile.mkstemp()
        biblr.app.testing = True
        self.app = biblr.app.test_client()
        with biblr.app.app_context():
            biblr.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(biblr.app.config['DATABASE'])


    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

	def get_db(self):
		'''Opens a new database connection if there is none yet for the
		current application context.

		'''
		if not hasattr(g, 'sqlite_db'):
			g.sqlite_db = self.connect_db()
		return g.sqlite_db

	def connect_db(self):
			'''Connects to the specific database.'''
			with biblr.app.app_context():
				rv = sqlite3.connect(biblr.app.config['DATABASE'])
				rv.row_factory = sqlite3.Row
				return rv

	def tearDown(self):
		os.close(self.db_fd)
		os.unlink(biblr.app.config['DATABASE'])

if __name__ == '__main__':
    unittest.main()
